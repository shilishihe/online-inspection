import requests


def send_ding(content,mobile):
    robot_url = "https://oapi.dingtalk.com/robot/send?access_token=bd92a2ab1bd3243084849ffb96506e1620359581b97b49bafe870ba640b014c1"
    robot_body = {
        "msgtype": "text",
        "text": {
            "content": content
        },
        "at": {
            "atMobiles": [
                mobile,

            ],
            "isAtAll": False
        }
    }
    r = requests.post (robot_url, json=robot_body)
    if r.status_code==200:
        return True
    else:
        return False