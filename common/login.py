import requests
import time
from common import log
import re
from common.readConfig import ReadConfig
import json
import os


global a
a = ReadConfig()
#收款宝登录
def skb_login():
    try:
        log.logger_info.info("----开始链接收款宝登录----")
        login_url = a.get_url("skb_login_url")
        body_login = a.get_body("skb_body_login")
        header = a.get_body("header")
        session = requests.Session ()
        cookie_jar = session.post (login_url, json=body_login, headers=header).cookies
        cookie = requests.utils.dict_from_cookiejar (cookie_jar)
        for cookie1 in cookie_jar:
            # 获取cookie的失效时间
            expiry_date = cookie1.expires
            # 转化为当前时间
            print (time.ctime (expiry_date))
        #将cookie值以及cookie过期时间写入文件中
        data_file = open (r"../testfile/skb_cookie.txt", "w")
        data_file.write (str (cookie))
        data_file.write("\n"+str(expiry_date))
        data_file.close ()
        log.logger_info.info ("----收款宝登录操作完成----")
        return cookie
    except Exception as ex:
        log.logger_error.error("收款宝登录失败:"+str(ex))
#实际获取收款宝cookie调用的方法
def skb_login_data():
    if os.path.exists(r"../testfile/skb_cookie.txt") is False:
        return skb_login ()
    else:
        fd = open (r"../testfile/skb_cookie.txt")
        #把txt文件按列表形式读取出来
        fd_len = len(fd.read())
        print(fd_len)
        if fd_len > 5 :
            #将指针归位
            fd.seek(0)
            data_txt = fd.readlines ()
            text = data_txt[0]
            # 把str类型的转换车dict类型，需要先把字符串的单引号转换成双引号
            cookie_text = json.loads (re.sub ('\'', '\"', text))
            expiry_date = int (data_txt[1])
            print (time.ctime (expiry_date))
            # 判断当前时间大于失效时间，重新获取cookie，否则继续使用原来的cookie
            if time.time () < expiry_date:
                return cookie_text
                fd.close()
            else:
               return skb_login()

        else:
           return skb_login()

#金管家登录
def jgj_login():
    try:
        log.logger_info.info ("----开始链接金管家登录----")
        login_url = a.get_url("jgj_login_url")
        sms_url = a.get_url("jgj_sms_url")
        body_smg = a.get_body("jgj_body_smg")
        body_login = a.get_body("jgj_body_login")
        header = a.get_body("header")
        # try:
        session = requests.Session ()
        r_smg = requests.post (sms_url, data=body_smg)
        result = r_smg.json()
        cookie_jar = session.post (login_url, json=body_login, headers=header).cookies
        cookie = requests.utils.dict_from_cookiejar (cookie_jar)
        for cookie1 in cookie_jar:
            # 获取cookie的失效时间
            expiry_date = cookie1.expires
            # 转化为当前时间
            print (time.ctime (expiry_date))
        #将cookie值以及cookie过期时间写入文件中
        data_file = open (r"../testfile/jgj_cookie.txt", "w")
        data_file.write (str (cookie))
        data_file.write("\n"+str(expiry_date))
        log.logger_info.info ("----金管家登录成功----")
        return cookie
        data_file.close ()

    except Exception as ex:
        log.logger_error.error("金管家登录失败:"+str(ex))



#实际获取金管家cookie调用的方法
def jgj_login_data():
    if os.path.exists(r"../testfile/jgj_cookie.txt") is False:
        return jgj_login()
    else:
        fd = open (r"../testfile/jgj_cookie.txt")
        #把txt文件按列表形式读取出来
        fd_len = len(fd.read())
        print(fd_len)
        if fd_len > 5 :
            #将指针归位
            fd.seek(0)
            data_txt = fd.readlines ()
            text = data_txt[0]
            # 把str类型的转换车dict类型，需要先把字符串的单引号转换成双引号
            cookie_text = json.loads (re.sub ('\'', '\"', text))
            expiry_date = int (data_txt[1])
            print (time.ctime (expiry_date))
            print("没有走")
            # 判断当前时间大于失效时间，重新获取cookie，否则继续使用原来的cookie
            if time.time () < expiry_date:
                fd.close()
                return cookie_text
            else:
                return jgj_login()
        else:
            return jgj_login()

