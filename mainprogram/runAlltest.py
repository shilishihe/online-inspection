import time
import unittest
from common import HTMLTestRunner
from testcase.shanyongbao_test import SybTest
from testcase.shoukuanbao_test import SkbTest

# #======查找测试报告目录，找到最新生成的测试报告文件====
# def send_report(testreport):
#     result_dir = testreport
#     lists = os.listdir (result_dir)
#     lists.sort (key=lambda fn: os.path.getmtime (result_dir + "\\" + fn))
#     # print (u'最新测试生成的报告： '+lists[-1])
#     # 找到最新生成的文件
#     file_new = os.path.join (result_dir, lists[-1])
#     print(file_new)
#     # 调用发邮件模块
#     sendemail(file_new)

# ================将用例添加到测试套件===========
# def creatsuite():
#     suite = unittest.TestSuite()
#     # 定义测试文件查找的目录
#     test_dir = './testcase'
#     # 定义测试文件查找的目录
#     discover = unittest.defaultTestLoader.discover (test_dir, pattern='*_test.py',top_level_dir=None)
#     print(discover)
#     # # discover 方法筛选出来的用例，循环添加到测试套件中
#     # for test_case in discover:
#     #     testunit.addTests(test_case)
#     return  discover


if __name__ == "__main__":
    now_time = time.strftime("%Y-%m-%d %H_%M_%S")
    testreport = ''
    filename = testreport + now_time + '_result.html'
    fp = open(filename, 'wb')
    suite = unittest.TestSuite()
    test_dir = './testcase'
    ls = [SybTest("test_loan"), SkbTest("test_order")]
    suite.addTests(ls)
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=u'自动化报告', description=u'用例执行情况：')
    runner.run(suite)
    fp.close()
