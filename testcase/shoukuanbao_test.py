import requests
from common.login import skb_login_data
from common.dingding import send_ding
from common import log
import unittest
from common.readConfig import ReadConfig
# import os,sys
# curPath = os.path.abspath(os.path.dirname(__file__))
# rootPath = os.path.split(curPath)[0]
# sys.path.append(rootPath)

global a, b
a = skb_login_data ()
b = ReadConfig()

order_url = b.get_url ("skb_order_url")



class SkbTest(unittest.TestCase):

    def setUp(self):
        self.case_name = " "

    def test_order(self):
        '''订单测试'''
        try:
            # self.order_url = b.get_url ("skb_order_url")
            r1 = requests.get (order_url, cookies=a)
            result = r1.json ()
            print (result)
            self.assertEqual (result["code"], 0)
            if result["code"] == 0:
                send_ding ("收款宝订单模块正常@13058019302", "13058019302")
                log.logger_info.info ("收款宝订单模块正常")
            else:
                send_ding ("系统异常:" + str (result) + "@13058019302", "13058019302")
                log.logger_error.error ( str (result))

        except Exception as ex:
            log.logger_error.error(ex)


